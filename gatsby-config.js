require("dotenv").config({
  path: `.env`,
})

module.exports = {

  plugins: [
    `gatsby-plugin-sharp`,
    `gatsby-transformer-sharp`,
    `gatsby-transformer-json`,
    `gatsby-plugin-sass`,
    `gatsby-image`,
    
    {
      resolve: `gatsby-source-filesystem`,
      options: {
        name: `images`,
        path: `${__dirname}/src/components/`,
        ignore: [`**/\.*`], // ignore files starting with a dot
      },
    },
    // {
    //   resolve: 'gatsby-source-rest-api',
    //   options: {
    //     endpoints: [
    //       //API endpoints
    //       'https://jsonplaceholder.typicode.com/posts',
    //       'https://jsonplaceholder.typicode.com/users',

    //     ],
    //   },
    // },
    {
      resolve: "gatsby-source-custom-api",
      options: {
          url: `${process.env.GATSBY_API_ENDPOINT}/${process.env.GATSBY_API_SITE_ID}/?format=json`
      },
      rootKey: "page",
      schemas: {
        website: `
            name: String
            company_name: String
            industry: String
            chosen_template: String
            primary_color: String
            secondary_color: String
            site_id: String
            domain_name: String
            page: [pages]
            index: Int

        `,
        page: `
            index: Int
            name: String
            slug: String
            section: [section]
        `,
        section: `
            index: Int
            section_type: String
            title: String
            text: String
            title_one: String
            title_two: String
            title_three: String
            text_one: String
            text_two: String
            text_three: String
            subtitle: String
            button_text: String
            phone: String
            phone_time: String
            phone_text: String
            email_text: String
            email: String
            image: String
            link: String
            link_text: String
            services: String
            embed: String
        `
      }
    },
    // {
    //   resolve: `gatsby-source-unsplash`,
    //   options: {
    //     appId: `12345678`,
    //     collections: [
    //       `098765`
    //     ],
    //     // optional: will only get page 1, so increase this count to include > 10 photos
    //     perPage: `100`
    //   },
    // },
  ]

}

import React from 'react'
import AnmeldHandvaerkerImage from './anmeldHaandvaerker.png'
import AnmeldHandvaerkerStyles from './anmeldHandvaerker.module.scss'

const AnmeldHandvaerker = (props) => {
    
    return (
        <section className={AnmeldHandvaerkerStyles.sectionPadding}>
            <div className="container">
                <div className="row">
                    <div className="col col-xs-12 text-center">
                        {props.title ? <h4>{props.title}</h4> : null }
                        {props.text ? <p>{props.text}</p> : null }
                        <img src={AnmeldHandvaerkerImage}></img>
                    </div>
                </div>
            </div>
        </section>
    )
}

export default AnmeldHandvaerker

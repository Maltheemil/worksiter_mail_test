import React from 'react'
import Img from 'gatsby-image'
import headerStyles from './header.module.scss'
import {getContrastColor, LightenColor} from '../../hoc/helpers'
import ContactModal from '../../modern/contactModal/contactModal'
import ContactForm from '../contactModal/contactForm/contactForm'


const Header = (props) => {


    return (
      <>
      <ContactModal 
        show={props.showModalState} 
        modalClosed={props.hideModal} 
      >
        <ContactForm
          primary={props.primary} 
          submitAssignmentHandler={props.submitAssignmentHandler}
          sendButtonText={props.sendButtonText}
        />
      </ContactModal>

        <section className={headerStyles.headerWrapper} style={{backgroundColor: props.lightColor}}>
          <div className="container">
            <div className={["row middle-xs", headerStyles.headerInner].join(' ')}>
              <div className={["col col-xs-12 col-lg-6 space-xs space-sm space-md last-xs first-lg", headerStyles.imageWrapper].join(' ')}>
                <img src={props.image} className={headerStyles.image}></img>
              </div>
                { props.title || props.text || props.buttonText ? 
                  <div className={["col col-xs-12 col-lg-6 first-xs last-lg", headerStyles.textWrapper].join(' ')}>
                    { props.title ? <h1>{props.title}</h1> : null }
                    { props.text ? <p>{props.text}</p> : null }
                    { props.buttonText ? <button className={headerStyles.button} onClick={props.showModal} style={{ backgroundColor: props.secondary ? props.secondary : '#ff7b00', color: getContrastColor(props.primary)}}>{props.buttonText}</button> : null }
                  </div>
                : null}
              </div>
            </div>
            {/* <svg xmlns="http://www.w3.org/2000/svg" preserveAspectRatio="xMidYMid slice" width="1366" height="94.383" viewBox="0 0 1366 94.383">
              <path id="Path_48" data-name="Path 48" d="M0,0H1366V94.383Z" fill="#f2f7fa"/>
            </svg> */}
                <svg className={headerStyles.svg} xmlns="http://www.w3.org/2000/svg"  preserveAspectRatio="xMidYMid slice" viewBox="0 0 560 28.28"><path d="M0,0H560V28.28Z" fill={props.lightColor}/></svg>
 
        </section>
      </>
        
    ) 
}

export default Header


   {/* props.image && props.image.childImageSharp ? <Img className={headerStyles.headerImage} fluid={props.image} /> : props.image ? <img className={headerStyles.headerImage} src={props.image} /> : null }
          <div className={[headerStyles.headerContent, "col col-xs-10 col-xs-offset-1"].join(' ')}>
            { props.title ? <h1 className={["text-center", "text-white", headerStyles.sectionName].join(' ')}>{props.title }</h1> : null }
            { props.text ? <p className="text-center text-white">{props.text}</p> : null }
            { props.buttonText ? <button onClick={props.showModal} style={{ backgroundColor: props.primary ? props.primary : '#ff7b00', color: getContrastColor(props.primary)}}>{props.buttonText}</button> : null }
    </div> */}

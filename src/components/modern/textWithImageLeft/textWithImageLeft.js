import React from 'react'
import textWithImageLeftStyles from './textWithImageLeft.module.scss'
import Img from 'gatsby-image'

export const TextWithImageLeftModern = (props) => {

    return (
        <section className={textWithImageLeftStyles.sectionPadding}>
            <div className="container">
                <div className="row middle-xs start-xs">
                    <div className="col col-xs-12 col-md-6">
                        { props.textTitle ? <h4 className={textWithImageLeftStyles.textTitle}>{props.textTitle}</h4> : null }
                        { props.text ? <p className={textWithImageLeftStyles.text}>{props.text}</p> : null }
                    </div>
                    <div className="col col-xs-12 col-md-6 first-xs last-md space-xs space-sm">
                    { props.image && props.image.childImageSharp ? 
                          <Img className={textWithImageLeftStyles.image} fluid={props.image.childImageSharp.fluid} /> 
                        : props.image ? <img className={textWithImageLeftStyles.image} src={props.image} />  
                    : null }
                    </div>
                </div>
            </div>
        </section> 
    )
}

export default TextWithImageLeftModern
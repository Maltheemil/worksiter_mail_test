import React from 'react'
import whyUsStyles from '../whyUs.module.scss'

const TextBlock = ({textBlocks, secondary}) => {

 
  return (
    <>
      {textBlocks.map((block, i) => {
        return (
          <div key={i} className={["col col-xs-12 col-md-6 col-lg-4 space-xs-up text-center", whyUsStyles.textBlock].join(' ')}>
            <div className={whyUsStyles.textWrapper}>
              <h5>{block.title}</h5>
              <p>{block.text}</p>
            </div>
          </div>
        )
      })}
    </>
  )
  
}

export default TextBlock

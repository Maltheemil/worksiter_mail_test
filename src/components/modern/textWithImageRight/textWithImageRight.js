import React from 'react'
import textWithImageRightStyles from './textWithImageRight.module.scss'
import Img from 'gatsby-image'

export const TextWithImageRightModern = (props) => {

    return (
        <section className={textWithImageRightStyles.sectionPadding}>
            <div className="container">
                <div className="row middle-xs start-xs">
                    <div className="col col-xs-12 col-md-6 space-xs space-sm space-md">
                        { props.image && props.image.childImageSharp ? 
                          <Img className={textWithImageRightStyles.image} fluid={props.image.childImageSharp.fluid} /> 
                        : props.image ? <img className={textWithImageRightStyles.image} src={props.image} />  
                        : null }
                    </div>
                    <div className={"col col-xs-12 col-md-6"}>
                        { props.textTitle ? <h4 className={textWithImageRightStyles.textTitle}>{props.textTitle}</h4> : null }
                        <p dangerouslySetInnerHTML={{__html: props.text}}></p>
                    </div>
                </div>
            </div>
        </section> 
    )
}

export default TextWithImageRightModern

import React from 'react'
import contactStyles from './contactForm.module.scss'
import {getContrastColor} from '../../../hoc/helpers'

const ContactForm = (props) => {

    return (
        <div className="col col-xs-12">
            <form className={contactStyles.form}>
                <input type="text" placeholder="Navn" className={contactStyles.input} />
                <input type="tel" placeholder="Telefonnummer" className={contactStyles.input} />
                <input type="email" placeholder="Email" className={contactStyles.input} />
                <textarea name="assignment" cols="30" rows="8"  placeholder="Beskriv opgaven..." className={contactStyles.input} />
            </form>
            <button className={contactStyles.button} style={{ backgroundColor: props.primary ? props.primary : '#ff7b00', color:  getContrastColor(props.primary)} } onClick={props.submitAssignmentHandler}>{props.sendButtonText}</button>
        </div>
    )
}

export default ContactForm
import React from 'react'
import contactModalStyles from './contactModal.module.scss'
import Backdrop from '../backdrop/backdrop'

const ContactModal = (props) => {
    return (
        <>
        <Backdrop show={props.show} clicked={props.modalClosed}/>
            <div className={contactModalStyles.Modal} 
                style={{
                    transform: props.show ? 'translateY(0)' : 'translateY(100vh)', 
                    opacity: props.show ? '1' : '0'
                }}
            >
            {props.children}
            </div>
        </>
    )
}

export default ContactModal

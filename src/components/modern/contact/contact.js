import React from 'react'
import contactStyles from './contact.module.scss'
import PhoneIcon from './phoneIcon'
import {getContrastColor} from '../../hoc/helpers'

const ContactModern = ({lightColor, setFullname, setPhone, setEmail, setMessage, primary, secondary, title, text, phone, submitAssignmentHandler, buttonText}) => {
          
    // A little help for the Netlify post-processing bots
    <form data-netlify="true" name="userEmail" method="post" netlify netlify-honeypot="bot-field" hidden>
        <input type="text" name="name" placeholder="Navn" className={contactStyles.input} onChange={(event) => setFullname({fullName: event.target.value})}/>
        <input type="tel" name="phone" placeholder="Telefonnummer" className={contactStyles.input} onChange={(event) => setPhone({phone: event.target.value})}/>
        <input type="email" name="email" placeholder="Email" className={contactStyles.input} onChange={(event) => setEmail({email: event.target.value})}/>
        <textarea name="assignment" cols="30" rows="8"  placeholder="Beskriv opgaven..." className={contactStyles.input} onChange={(event) => setMessage({message: event.target.value})}/>
    </form>

    return (
        <section className={[contactStyles.sectionPadding].join(' ')}>
            <div className="container">
                <div className={["row flex middle-xs", contactStyles.contactWrapper].join(' ')}>
                    <div className={["col col-xs-12 col-md-6", contactStyles.formWrapper].join(' ')} style={{backgroundColor: lightColor}}>
                        <form data-netlify="true" name="userEmail" method="POST" method="post" className={contactStyles.form}>
                            <input type="hidden" name="form-name" value="userEmail" />
                            <input type="text" name="name" placeholder="Navn" className={contactStyles.input} onChange={(event) => setFullname({fullName: event.target.value})}/>
                            <input type="tel" name="phone" placeholder="Telefonnummer" className={contactStyles.input} onChange={(event) => setPhone({phone: event.target.value})}/>
                            <input type="email" name="email" placeholder="Email" className={contactStyles.input} onChange={(event) => setEmail({email: event.target.value})}/>
                            <textarea name="assignment" cols="30" rows="8"  placeholder="Beskriv opgaven..." className={contactStyles.input} onChange={(event) => setMessage({message: event.target.value})}/>
                        </form>
                        <button type="submit" className={contactStyles.button} style={{ backgroundColor: primary ? primary : '#ff7b00', color: getContrastColor(primary)}} onClick={submitAssignmentHandler}>{buttonText}</button>
                    </div>
                    <div className={["col", "col-xs-12", "col-md-6", "space-xs-up", "text-center", contactStyles.conpanyContact].join(' ')}>
                    { title ? <h4 className="space-xs-up">{title}</h4> : null }
                        { text ? <p>{text}</p> : null }
                        { phone ? <PhoneIcon secondary={secondary}/> : null }
                        { phone ? <a style={ primary ? {color: primary} : {color: '#0283f1'} } href={`tel:${phone}`}>{phone}</a> : null }
                    </div>
                 
                </div>
            </div>
        </section>
    )
}

export default ContactModern


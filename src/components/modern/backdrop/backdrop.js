import React from 'react'
import backdropStyles from './bockdrop.module.scss'

const Backdrop = (props) => {
    return (
        <>
        { props.show ? <div className={backdropStyles.backdrop} onClick={props.clicked}></div> : null }
        </>     
    )
}

export default Backdrop

import React from 'react'
import contactSimpleStyles from './contactSimple.module.scss'
import PhoneIcon from './phoneIcon'
import EmailIcon from './emailIcon'

export const ContactSimpleModern = (props) => {
      
    return (
        <section className={[contactSimpleStyles.sectionPadding, contactSimpleStyles.bgImage].join(' ')} style={{backgroundColor: props.lightColor}}> 
            <div className="container"> 
                <div className="row middle-xs center center-xs">

                {props.phoneNumber ?
                  <div className={["col col-xs-12 col-md-4 col-lg-3 space-xs", contactSimpleStyles.phoneWrapper].join(' ')}>
                    <PhoneIcon {...props} />
                    <h5 className="center">{props.phoneText}</h5>
                    <a className={[contactSimpleStyles.link, "center"].join(' ')} href={`tel:${props.phoneNumber.split(' ').join('')}`} style={props.primary ? {color: props.primary} : null}>{props.phoneNumber}</a>
                    <p className="small center">{props.phoneTime}</p>
                  </div>
                : null }
                  
                {props.email ?
                  <div className={["col col-xs-12 col-md-4 col-lg-3", contactSimpleStyles.emailWrapper].join(' ')}>
                    <EmailIcon {...props} />
                    <h5 className="center">{props.emailText}</h5>
                    <a className={[contactSimpleStyles.link, "center"].join(' ')} href={`mailto:${props.email}`} style={props.primary ? {color: props.primary} : null}>{props.email}</a>
                  </div>
                : null }
                  
                </div>
            </div>
        </section> 
    )
}

export default ContactSimpleModern
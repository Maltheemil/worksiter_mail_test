import React from 'react'
import TextBlock from './textBlock/textBlock'
import footerStyles from './footer.module.scss'

const FooterModern = (props) => {
    
    return (
        <footer className={[footerStyles.sectionPadding, footerStyles.footer].join(' ')} style={{backgroundColor: props.lightColor}}>
            <div className="container">
                <div className="row">
                    <div className="col col-xs-12 text-center">
                        {props.textOne ?
                        <div className="col col-xs-12 col-md-6 col-lg-4 space-xs-up small">
                            <h6>{props.titleOne}</h6>
                            <p>{props.textOne}</p>
                        </div>
                        : null}
                        {props.textTwo ?
                        <div className="col col-xs-12 col-md-6 col-lg-4 space-xs-up small">
                            <h6>{props.titleTwo}</h6>
                            <p>{props.textTwo}</p>
                        </div>
                        : null}
                        {props.textThree ?
                        <div className="col col-xs-12 col-md-6 col-lg-4 space-xs-up small">
                            <h6>{props.titleThree}</h6>
                            <p>{props.textThree}</p>
                        </div>
                        : null}
                    </div>
                </div>
            </div>
        </footer>
    )
}

export default FooterModern

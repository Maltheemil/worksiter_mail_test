import React from 'react'
import spinnerStyles from './spinner.module.scss'

const Spinner = () => {
    return (
        <>
        <div className={spinnerStyles.ldsEllipsis}><div></div><div></div><div></div><div></div></div>
        </>
    )
}

export default Spinner

import React from 'react'
import contactStyles from './contact.module.scss'
import PhoneIcon from './phoneIcon'
import {getContrastColor} from '../../hoc/helpers'

const Contact = (props) => {

    return (
        <section className={[contactStyles.sectionPadding, contactStyles.bgGrey].join(' ')}>
            <div className="container">
                <div className={["row flex middle-xs", contactStyles.contactWrapper].join(' ')}>
                    <div className={["col", "col-xs-12", "col-lg-6", "space-xs-up", contactStyles.conpanyContact].join(' ')}>
                    { props.title ? <h4 className="space-xs-up">{props.title}</h4> : null }
                        { props.text ? <p>{props.text}</p> : null }
                        { props.phone ? <a style={ props.primary ? {color: props.primary} : {color: '#0283f1'} } href={`tel:${props.phone}`}><PhoneIcon {...props} />{props.phone}</a> : null }
                    </div>
                    <div className="col col-xs-12 col-lg-6">
                        <form className={contactStyles.form}>
                            <input type="text" placeholder="Navn" className={contactStyles.input} />
                            <input type="tel" placeholder="Telefonnummer" className={contactStyles.input} />
                            <input type="email" placeholder="Email" className={contactStyles.input} />
                            <textarea name="assignment" cols="30" rows="8"  placeholder="Beskriv opgaven..." className={contactStyles.input} />
                        </form>
                        <button className={contactStyles.button} style={{ backgroundColor: props.primary ? props.primary : '#ff7b00', color:  getContrastColor(props.primary)} } onClick={props.submitAssignmentHandler}>{props.sendContactButtonText}</button>
                        </div>
                </div>
            </div>
        </section>
    )
}

export default Contact


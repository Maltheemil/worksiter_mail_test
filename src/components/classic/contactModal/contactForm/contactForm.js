import React from 'react'
import contactStyles from './contactForm.module.scss'
import {getContrastColor} from '../../../hoc/helpers'

const ContactForm = (props) => {

    return (
        <div className="col col-xs-12">
            <form data-netlify="true" className={contactStyles.form} name="contact" method="POST">
                <input name="name" type="text" placeholder="Name" className={contactStyles.input} />
                <input name="tel" type="tel" placeholder="Tel" className={contactStyles.input} />
                <input name="email" type="email" placeholder="Email" className={contactStyles.input} />
                <textarea name="job" cols="30" rows="8"  placeholder="Beskriv opgaven..." className={contactStyles.input} />
                {/* <input type="file" className={contactStyles.fileUpload}/> */}
                <button type="submit" className={contactStyles.button} style={{ backgroundColor: props.primary ? props.primary : '#ff7b00', color:  getContrastColor(props.primary)} } onClick={props.submitAssignmentHandler}>{props.sendButtonText}</button>
                </form>
        </div>
    )
}

export default ContactForm
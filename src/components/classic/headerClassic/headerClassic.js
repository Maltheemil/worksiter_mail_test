import React from 'react'
import Img from 'gatsby-image'
import headerStyles from './headerClassic.module.scss'
import {getContrastColor} from '../../hoc/helpers'
import ContactModal from '../../classic/contactModal/contactModal'
import ContactForm from '../../classic/contactModal/contactForm/contactForm'

const HeaderClassic = (props) => {

    return (
      <>
      <ContactModal 
        show={props.showModalState} 
        modalClosed={props.hideModal} 
      >
        <ContactForm
          primary={props.primary} 
          submitAssignmentHandler={props.submitAssignmentHandler}
          sendButtonText={props.sendButtonText}
        />
      </ContactModal>

        <section className={headerStyles.headerWrapper}>
          { props.image && props.image.childImageSharp ? <Img className={headerStyles.headerImage} fluid={props.image} /> : props.image ? <img className={headerStyles.headerImage} src={props.image} /> : null }
          <div className={[headerStyles.headerContent, "col col-xs-10 col-xs-offset-1"].join(' ')}>
            { props.title ? <h1 className={["text-center", "text-white", headerStyles.sectionName].join(' ')}>{props.title }</h1> : null }
            { props.text ? <p className="text-center text-white">{props.text}</p> : null }
            { props.buttonText ? <button onClick={props.showModal} style={{ backgroundColor: props.primary ? props.primary : '#ff7b00', color: getContrastColor(props.primary)}}>{props.buttonText}</button> : null }
            </div>
        </section>
      </>
        
    )
}

export default HeaderClassic

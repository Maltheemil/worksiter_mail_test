import React from 'react'
import galleryStyles from './gallery.module.scss'
import GalleryImage from './gallerymage/galleryImage'

const Gallery = ({galleryImages, title, text}) => {

    return (
            <section className={galleryStyles.sectionPadding}>
                <div className="container">
                    <div className="row">
                    { title || text ?
                        <div className="col col-xs-12 space-xs-up text-center">
                            {title ? <h4>{title}</h4> : null }
                            {text ?  <p>{text}</p> : null }
                        </div>
                    : null }
                    { galleryImages ?
                        <div className="col col-xs-12">
                            <GalleryImage galleryImages={galleryImages}/>
                        </div>
                    : null }
                    </div>
                </div>
            </section>
    )
}

export default Gallery

import React from 'react'
import Img from "gatsby-image"
import galleryImageStyles from './galleryImage.module.scss'

const GalleryImage = ({galleryImages}) => {
  
    return (
        <>
          {galleryImages.map((image, i) => {
            return (
              <div key={i} className={["col col-xs-12 col-sm-6 col-lg-4 space-xs-up", galleryImageStyles.imageWrapper].join(' ')}>
                <a href={image} target="_blank"><img src={image} className={galleryImageStyles.image}/></a>
              </div>
            )
          })}
        </>
      )
}

export default GalleryImage

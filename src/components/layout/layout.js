import React, { useState } from 'react'
import "./style/app.scss"
import Navbar from './Navbar/Navbar'
import { useStaticQuery, graphql } from "gatsby"


const Layout = ( {children, lightColor} ) => {

    const data = useStaticQuery(graphql`
        query GlobalQuery {
            customApi {
                chosen_template
                company_name
                domain_name
                industry
                name
                primary_color
                secondary_color
                page {
                    name
                    index
                    slug
                }
            }
        }          
    `)

  
    const [navbarExpanded, setNavbarExpanded] = useState(false)

    console.log(data)
    return (
        <>
            <Navbar 
                menuItems={data.customApi.page}
                title={data.customApi.name}
                phone={"+45 23 37 37 24"}
                navbarExpanded={navbarExpanded}
                setNavbarExpanded={setNavbarExpanded}
                primary={data.customApi.primary_color}
                secondary={data.customApi.secondary_color}
                template={data.customApi.chosen_template}
                lightColor={lightColor}
            />
            <main>  
            {children}
        </main>
        </>
    )
}

export default Layout


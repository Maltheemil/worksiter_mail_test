import React, {useState} from "react"
import HeaderClassic from '../components/classic/headerClassic/headerClassic.js'
import Layout from '../../src/components/layout/layout.js'
// import Carpenter from '../components/classic/headerClassic/carpenter.png'
// import ImageFallback from '../../src/images/ImageFallback.jpg'
import TextWithImageRight from '../components/classic/textWithImageRight/textWithImageRight'
import TextWithImageRightModern from '../components/modern/textWithImageRight/textWithImageRight'
import TextWithImageLeft from '../components/classic/textWithImageLeft/textWithImageLeft'
import TextWithImageLeftModern from '../components/modern/textWithImageLeft/textWithImageLeft'
import ServicesList from '../components/classic/servicesList/servicesList'
import ServicesListModern from '../components/modern/servicesList/servicesList'
import WhyUs from '../components/classic/whyUs/whyUs'
import WhyUsModern from '../components/modern/whyUs/whyUs'
import ContactSimple from '../components/classic/contactSimple/contactSimple'
import ContactSimpleModern from '../components/modern/contactSimple/contactSimple'
import Contact from '../components/classic/contact/contact'
import ContactModern from '../components/modern/contact/contact'
import AnmeldHandvaerker from '../components/classic/anmeldHaandvaerker/anmeldHandvaerker'
import Code from '../components/classic/code/code'
import CodeModern from '../components/modern/code/code'
import Footer from '../components/classic/footer/footer'
import FooterModern from '../components/modern/footer/footer'
import Spinner from '../components/hoc/spinner/spinner'
import Checkmark from '../components/hoc/checkmark/checkmark'
import axios from 'axios'
import Navbar from '../components/layout/Navbar/Navbar'
import Text from '../components/classic/text/text'
import Gallery from '../components/classic/gallery/gallery'
import Header from '../components/modern/header/header'
import EmailIcon from '../components/hoc/emailIcon'

import tinycolor from 'tinycolor2'


const Page = ({pageContext}) => {

// Header form functionality
const [sendButtonText, setsendButtonText] = useState(<EmailIcon/>)
const [showModalState, setShowModalState] = useState(false)

const showModalHandler = () => {
  setShowModalState(true)
}

const hideModalHandler = () => {
  setShowModalState(false)
}

const submitModalFormHandler = (event) => {
    event.preventDefault();
    setsendButtonText(<Spinner />)
    // replace with Djangomail
      axios.post('https://jsonplaceholder.typicode.com/posts')
      .then(response => {
        if(response) {
          setsendButtonText(<Checkmark/>)
          setTimeout(hideModalHandler, 2300);
          }
      }).catch(error => {
          console.log(error);
          setsendButtonText('Error sending')
      })
}

// Contact Form functionality
const [sendContactButtonText, setSendContactButtonText] = useState(<EmailIcon/>)
const [fullName, setFullname] = useState('')
const [phone, setPhone] = useState('')
const [email, setEmail] = useState('')
const [message, setMessage] = useState('')

  const mail = {
    fullName,
    phone,
    email,
    message
  }

const submitContactFormHandler = (event) => {
    event.preventDefault();
    setSendContactButtonText(<Spinner />)
    // replace with Djangomail
    console.log(mail)
      axios.post('https://worksiter-bdf7d.firebaseio.com/Emails.json', mail)
      .then(response => {
        if(response) {
          setSendContactButtonText(<Checkmark/>)
          }
      }).catch(error => {
          console.log(error);
          setSendContactButtonText('Error sending')
      })
}

  const modern = pageContext.template === 'modern'
  const classic = pageContext.template === 'classic'

  const lightColor = tinycolor.mix(pageContext.secondary_color, '#ffffff', 92)

    return (
        <Layout lightColor={lightColor}>
          {pageContext.sections.sort((a,b) => (a.index > b.index) ? 1 : ((b.index > a.index) ? -1 : 0)).map((s, index) => 
              s.section_type === 'header' && classic ?
              <HeaderClassic key={index} title={s.title} text={s.subtitle} image={s.image} buttonText={s.link_text} primary={pageContext.primary_color} secondary={pageContext.secondary_color} sendButtonText={sendButtonText} submitAssignmentHandler={submitModalFormHandler} showModal={showModalHandler} hideModal={hideModalHandler} showModalState={showModalState} />
            : s.section_type === 'header' && modern ?
              <Header key={index} title={s.title} text={s.subtitle} image={s.image} buttonText={s.link_text} primary={pageContext.primary_color} secondary={pageContext.secondary_color} sendButtonText={sendButtonText} submitAssignmentHandler={submitModalFormHandler} showModal={showModalHandler} hideModal={hideModalHandler} showModalState={showModalState} lightColor={lightColor}/>
            : s.section_type === 'services' && classic ?
              <ServicesList key={index} title={s.title} services={s.services.split(", ")} primary={pageContext.primary_color} secondary={pageContext.secondary_color} />
            : s.section_type === 'services' && modern ?
              <ServicesListModern key={index} title={s.title} services={s.services.split(", ")} primary={pageContext.primary_color} secondary={pageContext.secondary_color} lightColor={lightColor}/>
            : s.section_type === 'image_text_right' && classic ?
              <TextWithImageRight key={index} textTitle={s.title} text={s.subtitle} image={s.image}/>
            : s.section_type === 'image_text_right' && modern ?
              <TextWithImageRightModern key={index} textTitle={s.title} text={s.subtitle} image={s.image}/>
            : s.section_type === 'image_text_left' && classic ?
              <TextWithImageLeft key={index} textTitle={s.title} text={s.subtitle} image={s.image}/>
            : s.section_type === 'image_text_left' && modern ?
              <TextWithImageLeftModern key={index} textTitle={s.title} text={s.subtitle} image={s.image}/>
            : s.section_type === 'contact_section' && classic ?
              <Contact key={index} title={s.title} buttonText={s.button_text} text={s.text} phone={s.phone} primary={pageContext.primary_color} secondary={pageContext.secondary_color} sendContactButtonText={sendContactButtonText} submitAssignmentHandler={submitContactFormHandler}/>
            : s.section_type === 'contact_section' && modern ?
              <ContactModern setFullname={setFullname} setPhone={setPhone} setEmail={setEmail} setMessage={setMessage} key={index} title={s.title} buttonText={s.button_text} text={s.text} phone={s.phone} primary={pageContext.primary_color} secondary={pageContext.secondary_color} sendContactButtonText={sendContactButtonText} submitAssignmentHandler={submitContactFormHandler} lightColor={lightColor}/>
            : s.section_type === 'contact_simple' && classic ?
              <ContactSimple key={index} phoneText={s.phone_text} phoneNumber={s.phone} phoneTime={s.phone_time} emailText={s.email_text} email={s.email} primary={pageContext.primary_color} secondary={pageContext.secondary_color} />
            : s.section_type === 'contact_simple' && modern ?
              <ContactSimpleModern key={index} phoneText={s.phone_text} phoneNumber={s.phone} phoneTime={s.phone_time} emailText={s.email_text} email={s.email} primary={pageContext.primary_color} secondary={pageContext.secondary_color} lightColor={lightColor}/>
            : s.section_type === 'footer' && classic ?
              <Footer key={index} titleOne={s.title_one} textOne={s.text_one} titleTwo={s.title_two} textTwo={s.text_two} titleThree={s.title_three} textThree={s.text_three} />
            : s.section_type === 'footer' && modern ?
              <FooterModern key={index} titleOne={s.title_one} textOne={s.text_one} titleTwo={s.title_two} textTwo={s.text_two} titleThree={s.title_three} textThree={s.text_three} lightColor={lightColor}/>
            : s.section_type === 'why_us' && classic ?
              <WhyUs key={index} title={s.title} text={s.text} textBlocks={[s.text_one ? {title: s.title_one, text: s.text_one} : null, s.text_two ? {title: s.title_two, text: s.text_two} : null, s.text_three ? {title: s.title_three, text: s.text_three} : null]} primary={pageContext.primary_color} secondary={pageContext.secondary_color}/>
            : s.section_type === 'why_us' && modern ?
              <WhyUsModern key={index} title={s.title} text={s.text} textBlocks={[s.text_one ? {title: s.title_one, text: s.text_one} : null, s.text_two ? {title: s.title_two, text: s.text_two} : null, s.text_three ? {title: s.title_three, text: s.text_three} : null]} primary={pageContext.primary_color} secondary={pageContext.secondary_color}/>
            : s.section_type === 'code' && classic ?
              <Code key={index} title={s.title} text={s.text} embed={s.embed} />
            : s.section_type === 'code' && modern ?
              <CodeModern key={index} title={s.title} text={s.text} embed={s.embed} />
            : null
          )}
        </Layout>
    )
}

export default Page
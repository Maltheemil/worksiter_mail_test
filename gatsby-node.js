const path = require("path");

exports.createPages = async ({ graphql, actions }) => {
    const { createPage } = actions;
    const result = await graphql(`
        {
            customApi {
                chosen_template
                company_name
                domain_name
                industry
                name
                primary_color
                secondary_color
                site_id
                page {
                  name
                  index
                  slug
                  section {
                    button_text
                    email
                    email_text
                    embed
                    image
                    index
                    link_text
                    phone
                    phone_text
                    phone_time
                    section_type
                    services
                    text
                    subtitle
                    text_one
                    text_three
                    text_two
                    title
                    title_one
                    title_three
                    title_two
                  }
                }
              }
        }
    `);
    return Promise.all(
        result.data.customApi.page.map(async page => {
            const siteData = result.data.customApi

            await createPage({
                path: page.slug === "__home__" ? "/" : page.slug,
                component: path.resolve("./src/templates/page.js"),
                context: {
                    // Data passed to context is available
                    // in page queries as GraphQL variables.
                    slug: page.slug === "__home__" ? "/" : page.slug,
                    index: page.index,
                    sections: page.section,
                    title: page.name,
                    template: siteData.chosen_template,
                    // chosen_template: siteData.chosen_template,
                    // company_name: siteData.company_name,
                    // domain_name: siteData.domain_name,
                    // industry: siteData.industry,
                    // name: siteData.name,
                    primary_color: siteData.primary_color,
                    secondary_color: siteData.secondary_color
                }
            });
        })
    );
};